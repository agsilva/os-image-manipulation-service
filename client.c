///Ana Rita Pascoa 2010129292		Effort: 30h
///Filipe Eduardo Catalao 2007183627
#include <sys/stat.h>
#include <sys/mman.h>	
#include <fcntl.h>
#include <stdio.h>	
#include <stdlib.h>	
#include <string.h>	
#include <unistd.h>
#include <signal.h>

#define	FILE_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

#ifndef	MAP_FILE	/* 44BSD defines this & requires it to mmap files */
#define	MAP_FILE	0	/* to compile under systems other than 44BSD */
#endif

#define DEBUG

#define PIPE_NAME "client_server"
#define SIZE 50
typedef struct{
	pid_t id;
	char fp[SIZE];
	int rot;
}Data;

void done(){
	printf("The image has been succesfully rotated!\n");
}

void error(){
	printf("Something went wrong...\n");
}
int get_stat(int fdin)
{
	struct stat pstatbuf;	
	if (fstat(fdin, &pstatbuf) < 0)	/* need size of input file */
	{
		fprintf(stderr,"fstat error\n");
		exit(1);
	}
	return pstatbuf.st_size;
}

int main(int argc, char *argv[])
{
	int fd,size,fdin;
	char *src;

	sigset_t mask;
	sigfillset(&mask);
	sigdelset(&mask,SIGUSR1);
	sigdelset(&mask,SIGUSR2);
	sigprocmask(SIG_SETMASK,&mask,NULL);

	if (signal(SIGUSR1, done) == SIG_ERR)
		printf("\nCan't catch SIGUSR1...\n");
	
	if (signal(SIGUSR2, error) == SIG_ERR)
		printf("\nCan't catch SIGUSR2...\n");

	if(argc != 3)
	{
		printf("Incorrect usage.\nPlease use \"./client filename rotation(90/180/270)\n");
		exit(1);
	}

	if (strcmp(argv[2],"90")!=0 && strcmp(argv[2],"180")!=0 && strcmp(argv[2],"270")!=0)
	{
		printf("Rotation must be 90, 180 or 270\n");
		exit(1);
	}

	if ( (fdin = open(argv[1], O_RDONLY)) < 0)
	{
		fprintf(stderr,"Can't open %s for reading\n", argv[1]);
		exit(1);
	}

	size = get_stat(fdin);

	if ( (src = mmap(0, size, PROT_READ, MAP_FILE | MAP_SHARED, fdin, 0)) == (caddr_t) -1)
	{
		fprintf(stderr,"Mmap error for input\n");
		exit(1);
	}


	if ((fd=open(PIPE_NAME, O_WRONLY)) < 0)
	{
		perror("Cannot open pipe for writing: ");
		exit(1);
	}

	Data info;
	info.id=getpid();
	strcpy(info.fp,argv[1]);
	info.rot=atoi(argv[2])/90-1;

#ifdef DEBUG
	printf("About to send: %d %s %d\n",info.id,info.fp,info.rot);
#endif
	write(fd, &info, sizeof(Data));
	
	pause();
	close(fd);
	munmap(src,size);
	close(fdin);

	return 0;
}
