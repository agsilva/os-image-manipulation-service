SVR_OBJECT_FILES = server.o
CLT_OBJECT_FILES = client.o  

CFLAGS = -Wall 
LIBS = -pthread
CC = gcc  

all: server client 
server: $(SVR_OBJECT_FILES)  
client: $(CLT_OBJECT_FILES) 

server client:
	$(CC) $(CFLAGS) $^ $(LIBS) -o $@  

%.o: %.c
	$(CC) -c $<  

clean: 
	rm -f server client *.o *~ 

