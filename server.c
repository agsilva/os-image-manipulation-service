///Ana Rita Pascoa 2010129292		Effort: 30h
///Filipe Eduardo Catalao 2007183627
#include <stdio.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <sys/mman.h>	
#include <string.h>
#include <sys/wait.h>
#include <stdlib.h>	
#include <pthread.h>
#include <errno.h>
#include <semaphore.h>

#define	FILE_MODE	(S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH)

#ifndef	MAP_FILE	/* 44BSD defines this & requires it to mmap files */
#define	MAP_FILE	0	/* to compile under systems other than 44BSD */
#endif

#define DEBUG

#define PIPE_NAME   "client_server"
#define NUM_WORKERS 4
#define NUM_PROCS 3
#define SIZE 50
#define BUFFSIZE 10

struct stat statbuf;

struct pixel
{
	char R;
	char G;
	char B;
};

typedef struct {
	pid_t id;
	char fp[SIZE];
	int r; //rotation or return from workerThread
}Data;

/* circular buffer */
typedef struct {
	pthread_mutex_t mutex;
	pthread_cond_t notFull; /*condition vbl*/
	pthread_cond_t notEmpty;
	int cnt /*num items in buf*/, frst, last;
	Data buf[BUFFSIZE];
} buffer_t;

char names[NUM_PROCS][10];
sem_t *can_read[NUM_PROCS];
pid_t procs[NUM_PROCS];
buffer_t theBuffer[NUM_PROCS]; 
int unn_pipe[NUM_PROCS][2][2];  
int id[NUM_WORKERS+1];
int fd;

pthread_t masterThr, workerThr[NUM_WORKERS];
sigset_t block_ctrlc;


int get_stat(int fdin)
{
	struct stat pstatbuf;	
	if (fstat(fdin, &pstatbuf) < 0)	/* need size of input file */
	{
		fprintf(stderr,"fstat error\n");
		exit(1);
	}
	return pstatbuf.st_size;
}

void terminate(int opt)															
{
	int i;
#ifdef DEBUG
	printf("Closing semaphores, unnamed pipes and childs...\n");
#endif
	for(i=0; i<NUM_PROCS; i++)												
	{
		sem_close(can_read[i]);
		sem_unlink(names[i]);
		close(unn_pipe[i][1][0]);
		close(unn_pipe[i][0][1]);
		kill(procs[i], SIGKILL);
	}
#ifdef DEBUG
	sleep(1);
	printf("Destroying pthread mutexes...\n");
#endif
	for(i=0; i<NUM_PROCS; i++)	
		pthread_mutex_destroy(&theBuffer[i].mutex);

#ifdef DEBUG	
	sleep(1);
	printf("Closing named pipe...\n");
#endif
	close(fd);
#ifdef DEBUG
	sleep(1);
#endif
	printf("SERVER OFF.\n");
	wait(NULL);
	exit(opt);
}
void sig_handler(int signum) {

	//int i,j;
	printf("\n ***  ^C pressed.  ***\n");
/*	for (i=0;i<NUM_PROCS;i++)
		if (theBuffer[i].cnt!=0){
			j=theBuffer[i].frst-1;
			printf("Tou aqui\n");
			do{
				j++;
				j%= BUFFSIZE;
				kill(theBuffer[i].buf[j].id,SIGUSR2);	
			}while(j!=theBuffer[i].last);
		}*/
	terminate(0);
		
}

void init(){
	int i;
	
	// Creates the named pipe if it doesn't exist yet
	unlink(PIPE_NAME);	
	if (mkfifo(PIPE_NAME, O_CREAT|O_EXCL|0666)<0)
	{
		perror("Cannot create pipe: ");
		exit(1);
	}

	// Opens the pipe for reading
	if ((fd=open(PIPE_NAME, O_RDONLY|O_NONBLOCK)) < 0)
	{
		perror("Cannot open pipe for reading: ");
		exit(1);
	}
	
	for (i=0; i<NUM_PROCS;i++){
		if (pipe(unn_pipe[i][0]) ) {
			fprintf(stderr,"Pipe error!\n");
			exit(1);
		}
		if (pipe(unn_pipe[i][1]) ) {
			fprintf(stderr,"Pipe error!\n");
			exit(1);
		}

		theBuffer[i].cnt = 0;
		pthread_mutex_init(&theBuffer[i].mutex, NULL);
		sprintf(names[i],"CAN_READ%d",i);
		sem_unlink(names[i]);
		if (((can_read[i]=sem_open(names[i],O_CREAT|O_EXCL,0700,0))<0) && (errno!= EEXIST))
		{
			perror("Cannot create semaphore");
			exit(1);
		}
	}

}


/***LEITURA E ESCRITA DE PIXEIS***/
struct pixel *get_pixel(char *buf, int *pos)
{
	struct pixel pix;
	struct pixel *ppix = &pix;
	ppix->R = buf[*pos];
	ppix->G = buf[(*pos)+1];
	ppix->B = buf[(*pos)+2];
	(*pos) += 3;
	return ppix;
}

void write_pixel(struct pixel *ppix, char *buf, int *pos)
{
	buf[*pos] = ppix->G;
	buf[(*pos)+1] = ppix->B;
	buf[(*pos)+2] = ppix->R;
	(*pos) += 3;
}
/**********************************/

/***LEITURA E ESCRITA DO BUFFER***/
void store(Data pItem, buffer_t *pb) {
	pthread_mutex_lock(&pb->mutex);

	while (pb->cnt == BUFFSIZE) /*buffer full*/
		pthread_cond_wait(&pb->notFull, &pb->mutex);
	
	pb->buf[pb->last] = pItem; /*put data item in buffer... */
	pb->last++; 
	pb->last %= BUFFSIZE; /*... & update last ptr */
	pb->cnt++;
	pthread_cond_signal(&pb->notEmpty);
	pthread_mutex_unlock(&pb->mutex);
}

Data retrieve(buffer_t *pb) {
	Data pItem;

	pthread_mutex_lock(&pb->mutex);

	while (pb->cnt == 0) /*buffer empty*/
		pthread_cond_wait(&pb->notEmpty, &pb->mutex);
		
	pItem = pb->buf[pb->frst]; /*get data from buffer...*/
	pb->frst++; pb->frst %= BUFFSIZE; /*... & update first ptr */
	pb->cnt--;
	
	pthread_cond_signal(&pb->notFull);
	pthread_mutex_unlock(&pb->mutex);
	
	return pItem;
}
/**********************************/


/* core routine for MASTER thread */
void *master(void *n) {
	int my_id=*((int*)n);
	Data info;
#ifdef DEBUG	
	printf("MasterThread %d created.\n",my_id);
	pthread_sigmask(SIG_BLOCK,&block_ctrlc,NULL);
#endif	
	while (1) {
		sem_wait(can_read[my_id]);
		pthread_sigmask(SIG_BLOCK,&block_ctrlc,NULL);
		read(unn_pipe[my_id][0][0], &info, sizeof(Data));
	#ifdef DEBUG	
		printf("[RECIEVED BY MASTER %d] -> Pid = %d   File = %s   Rotation = %d\n",my_id,info.id,info.fp,info.r);
		sleep(2);
	#endif
		store(info, &theBuffer[my_id]);
		pthread_sigmask(SIG_UNBLOCK,&block_ctrlc,NULL);
		sigprocmask(SIG_UNBLOCK,&block_ctrlc,NULL);

	}

}
// routine given by one of the demos
int do_rotation(Data pItem){
	int	fdin, fdout;
	char *src, *dst;
	char fich[SIZE];
	char filename[SIZE];
	int size;

	int counter,index, pos, x, y, i;
	int xmax = 0;
	int ymax = 0;
	int colormax = 0;
	
	/* mapeia ficheiro original */
	if ( (fdin = open(pItem.fp, O_RDONLY)) < 0)
	{
		fprintf(stderr,"Can't open %s for reading\n", pItem.fp);
		return 1;
	}

	size = get_stat(fdin);

	if ( (src = mmap(0, size, PROT_READ, MAP_FILE | MAP_SHARED, fdin, 0)) == (caddr_t) -1)
	{
		fprintf(stderr,"mmap error for input\n");
		return 1;
	}	
	
	
	/* Construção do nome do ficheiro de saída */
	for(i=0;;i++){
		if (pItem.fp[i]=='.')
			break;
		filename[i]=pItem.fp[i];
	}
	filename[i]='\0';
	sprintf(fich, "%s%d.ppm", filename,(pItem.r+1)*90);


	/* mapeia output file */
	if ( (fdout = open(fich, O_RDWR | O_CREAT | O_TRUNC,FILE_MODE)) < 0)
	{
		fprintf(stderr,"can't creat %s for writing\n", fich);
		return 1;
	}

	/* set size of output file */
	if (lseek(fdout, size - 1, SEEK_SET) == -1)
	{
		fprintf(stderr,"lseek error\n");
		return 1;
	}
	if (write(fdout, "", 1) != 1)
	{
		fprintf(stderr,"write error\n");
		return 1;
	}
	
	if ( (dst = mmap(0, size, PROT_READ | PROT_WRITE,MAP_SHARED,fdout, 0)) == (caddr_t) -1)
	{
		fprintf(stderr,"mmap error for output\n");
		return 1;
	}

	/*faz a rotação */
	sscanf(src,"P6\n%d %d\n%d\n",&xmax,&ymax,&colormax);

	
	struct pixel imagem [ymax][xmax];
	
	for (counter=0, index=0; counter<3;index++)
	{
		if (src[index]=='\n')
			++counter;
	} 	
	pos=index-1;

	for (y=0;y<ymax;y++)
		for (x=0;x<xmax;x++){
			imagem[y][x] = *(get_pixel(src,&pos));
	}
	pos=index;
	
	switch (pItem.r)
	{
		case 0 :
			//Rotação 90º clockwise
			sprintf(dst,"P6\n%d %d\n%d\n",ymax,xmax,colormax);
			for (x=0; x<xmax; x++)
				for (y=ymax-1; y> -1;y--)
					write_pixel(&(imagem[y][x]),dst,&pos);
			break;
		case 1:
			//Rotação 180º
			sprintf(dst,"P6\n%d %d\n%d\n",xmax,ymax,colormax);
			for (y=ymax-1;y>-1;y--)
				for (x = xmax-1; x>-1; x--)
					write_pixel(&(imagem[y][x]),dst,&pos);
			break;
		case 2:
			//Rotação 270º clockwise
			sprintf(dst,"P6\n%d %d\n%d\n",ymax,xmax,colormax);
			for (x=xmax-1; x>-1; x--)
				for (y=0; y<ymax;y++)
					write_pixel(&(imagem[y][x]),dst,&pos);
			break;		
	}
	
	/* clean */
	munmap(dst,size);
	munmap(src,size);
	close(fdout);
	close(fdin);
	
	return 0;

}

/* core routine for WORKER thread */
void *worker(void *rotation) {
	Data pItem;
	int my_id = *((int*)rotation);
#ifdef DEBUG	
	printf("WorkerThread from MasterThread %d created.\n",my_id);
#endif	
	pthread_sigmask(SIG_BLOCK,&block_ctrlc,NULL);
	while (1) { /* loop forever */
		pthread_sigmask(SIG_BLOCK,&block_ctrlc,NULL);
		pItem = retrieve(&theBuffer[my_id]);
	#ifdef DEBUG	
		printf("[RECIEVED BY WORKER OF MASTER %d] -> Pid = %d   File = %s   Rotation = %d\n",my_id,pItem.id,pItem.fp,pItem.r);
	#endif
		pItem.r=do_rotation(pItem);
	#ifdef DEBUG
		sleep(1);	
		printf("[SEND TO CHILD %d] -> Pid = %d   File = %s   Return = %d\n",my_id,pItem.id,pItem.fp,pItem.r);
		sleep(1);
	#endif
		write(unn_pipe[my_id][1][1],&pItem,sizeof(Data));
		pthread_sigmask(SIG_UNBLOCK,&block_ctrlc,NULL);
	}

}

int main(int argc, char *argv[])
{
	int i,j;
	fd_set read_set;
	Data buf;
   
	printf("Server ON.\n");	

	/*inicializacao de sinais*/	
	sigfillset(&block_ctrlc);
	sigdelset(&block_ctrlc,SIGINT);
	sigprocmask(SIG_SETMASK,&block_ctrlc,NULL); //"ignora"/bloqueia todos os sinais excepto sigint
	//sigprocmask (SIG_BLOCK,&block_ctrlc, NULL); //para bloquear o sigint em zonas criticas

	init();
	
	for (i=0; i< NUM_PROCS;i++)
	{
		if ((procs[i] = fork()) == 0)
		{
			close(unn_pipe[i][0][1]);
			close(unn_pipe[i][1][0]);
			sigprocmask(SIG_BLOCK,&block_ctrlc,NULL);
			id[NUM_WORKERS]=i;
			pthread_create(&masterThr, NULL, master, &id[NUM_WORKERS]);
			
			for (j=0; j <NUM_WORKERS; j++){
				id[j]=i;
				pthread_create(&workerThr[j], NULL, worker, &id[j]);
			}
			
			for (i=0; i<NUM_WORKERS; i++)
				pthread_join (workerThr[i], NULL);
			pthread_exit(NULL);		
			exit(0);
		}
		else if (procs[i] == -1)
		{
			perror("Error creating child process\n");
			exit(1);
		}
	}
	
	sigaddset(&block_ctrlc,SIGINT);
	sigprocmask (SIG_UNBLOCK,&block_ctrlc, NULL);
	if (signal(SIGINT, sig_handler) == SIG_ERR){
		printf("\nCan't catch SIGINT...\n");
		terminate(1);	
	}
	
	while (1)
	{   
		FD_ZERO(&read_set);
		
		FD_SET(fd,&read_set);
	    for (i=0; i<NUM_PROCS; i++)
			FD_SET(unn_pipe[i][1][0], &read_set);
		if(select(unn_pipe[2][1][0]+1,&read_set,NULL,NULL,NULL)>0){ //À escuta
			if(FD_ISSET(fd,&read_set)){
				sigprocmask(SIG_BLOCK,&block_ctrlc,NULL);
				while (read(fd, &buf, sizeof(Data))>0){
				#ifdef DEBUG	
					printf("\n\nRecieved something from named pipe!\n");
					printf("[Buff_Named_Pipe] -> id = %d filename = %s rotation = %d\n", buf.id,buf.fp,buf.r);
				#endif
					write(unn_pipe[buf.r][0][1],&buf,sizeof(Data));
					sem_post(can_read[buf.r]);
				}
				close(fd);
				if((fd=open(PIPE_NAME, O_RDONLY|O_NONBLOCK))<0){
					perror("Cannot open pipe for reading: ");
					terminate(1);
				}			
				sigprocmask(SIG_UNBLOCK,&block_ctrlc,NULL);
			}	
			for(i=0;i<NUM_PROCS;i++)
				if (FD_ISSET(unn_pipe[i][1][0], &read_set)){
				#ifdef DEBUG	
					printf("Recieved something from child %d!\n",i);
				#endif
					sigprocmask(SIG_BLOCK,&block_ctrlc,NULL);
					read(unn_pipe[i][1][0],&buf,sizeof(Data));
				#ifdef DEBUG	
					printf("[Buff_UNN_PIPE] -> %d %s %d\n", buf.id,buf.fp,buf.r);
					sleep(2);
				#endif
					if(buf.r==0)
						kill(buf.id,SIGUSR1);
					else
						kill(buf.id,SIGUSR2);
							
					FD_CLR(unn_pipe[i][1][0], &read_set);	
					sigprocmask(SIG_UNBLOCK,&block_ctrlc,NULL);
				}
		}

	}
	terminate(0);

	return 0;   
}
